// import logo from './logo.svg';
import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
// import { HashRouter as Router } from 'react-router-dom';

// import useToken from './components/App/useToken';
import Navbar from './components/molecules/Navbar/index.js';
import Login from './components/molecules/Login/index.js';
import Initial from './components/organisms/initial/index.js';

// function setToken(userToken) {
//   sessionStorage.setItem('token', JSON.stringify(userToken));
// }

// function geLoginToken() {
//   const tokenString = sessionStorage.getItem('token');
//   const userToken = JSON.parse(tokenString);
//   return userToken?.token
// }

function App() {
  // const token = getToken();
  const [token, setToken] = useState();
  //if(!token) {
//return <Login setToken={setToken} />
  //}
  return (
    <div className="wrapper">
      <BrowserRouter>
      <Navbar />
        <Routes>
          <Route path="/" element={<Initial />}/>
        </Routes>
      </BrowserRouter>
      <h1>Application</h1>
    </div>
  );
}

export default App;
