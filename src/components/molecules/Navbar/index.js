import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './index.css';


function Navbar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  return (
    <>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                oi
              </Link>
            </li>
          </ul>
        </nav>
    </>
  );
}

export default Navbar;
